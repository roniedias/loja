# README #

Servidor remoto para testes:
http://52.67.83.75/loja/

**Obs:** O IP: 54.207.26.37 foi **DESATIVADO!** utilize para realização de testes, o IP acima (52.67.83.75). 

Ao acessar o endereço acima, é carregada a página inicial da aplicação contendo uma lista com todos os produtos cadastrados na base de dados, conforme:

>

![img1.png](https://bitbucket.org/repo/oa9zp9/images/4076594087-img1.png)


>


Basta clicar nos produtos desejados para adicioná-los ao carrinho de compras. A quantidade de produtos é incrementada a cada seleção:


![img2.png](https://bitbucket.org/repo/oa9zp9/images/2554118324-img2.png)

>

Ao efetuar login, o usuário é direcionado para uma página contendo todos os itens contidos em seu carrinho:

![img3.png](https://bitbucket.org/repo/oa9zp9/images/41186840-img3.png)

>

![img4.png](https://bitbucket.org/repo/oa9zp9/images/3562015555-img4.png)

>

Para visualizar todos os pedidos previamente efetuados pelo usuário, basta clicar em "Meus pedidos":

![img5.png](https://bitbucket.org/repo/oa9zp9/images/647268988-img5.png)

>

Ao clicar em "Salvar", um novo pedido é gravado na base de dados