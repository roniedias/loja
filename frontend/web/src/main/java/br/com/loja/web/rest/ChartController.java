package br.com.loja.web.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.loja.backend.entity.Product;
import br.com.loja.backend.service.ChartService;


@Path("chart")
@Produces(MediaType.APPLICATION_JSON)
public class ChartController {
	
	@EJB
	ChartService chartService;
	
	@Context
	HttpServletRequest request;
	
	//private static final Logger log = LoggerFactory.getLogger(ChartController.class);
	
	
	//http://APP_URL/loja/api/rest/chart
	@GET
	@Path("")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response listChartProducts() {
		List<Product> products = chartService.listChartProducts(request);
		return Response.status(Status.OK).entity(products).build();
	}

	
	
	//http://APP_URL/loja/api/rest/chart/add
	/* In body:
	 * {
		"productId": "1",
		"description": "Produto 1",
		"price": 60,
		"stock": 15
	   }
	 */
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addProductToChart(Product product) {
		List<Product> products = chartService.addProductToChart(product, request);
		return Response.status(Status.OK).entity(products).build();
	}
	
	
	
	//http://APP_URL/loja/api/rest/chart/remove
	@DELETE
	@Path("/remove/all")
	public Response removeChart() {
		boolean removed = chartService.removeChart(request);
		return Response.status(Status.OK).entity("{ \"removed\" : " + removed + "}").build();
	}
	
	
	//http://APP_URL/loja/api/rest/chart/remove?productId=1
	// Obs: Returns items remaining in chart, after removal
	@DELETE
	@Path("/remove")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeProductFromChart(@QueryParam("productId") int productId) {
		List<Product> products = chartService.removeProductFromChart(productId, request);
		return Response.status(Status.OK).entity(products).build();
	}

	//http://APP_URL/loja/api/rest/chart/quantity
	@GET
	@Path("/quantity")
	public Response numberOfProducts() {
		return Response.status(Status.OK).entity("{\"quantity\": " +  chartService.numberOfProducts(request) + "}").build();
	}
	


}
