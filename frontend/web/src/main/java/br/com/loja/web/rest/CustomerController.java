package br.com.loja.web.rest;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.loja.backend.entity.Customer;
import br.com.loja.backend.service.CustomerService;


@Path("customer")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerController {
	
	@EJB
	private CustomerService customerService;
	
	@PersistenceContext
	private EntityManager em;

	
	// http://APP_URL/loja/api/rest/customer
	@GET
	@Path("")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response listCustomers() {
		return Response.status(Status.OK).entity(customerService.listCustomers()).build();
	}	
	
	// http://APP_URL/loja/api/rest/customer/find/1
	@GET 
	@Path("/find/{customerId}")
	public Response findCustomerById(@PathParam("customerId") long customerId) {
		Customer customer = customerService.findCustomerById(customerId);
		long c = customer.getCustomerId();
		Status status;
		if(c == 0) {
			status = Status.BAD_REQUEST;	
		}
		else {
			status = Status.OK;
		}
		return Response.status(status).entity(customer).build();
	}
	
	
	@GET
	@Path("/find_by_name/{name}")
	public Response findCustomerByName(@PathParam("name") String name) {
		Customer customer = customerService.findCustomerByEmail(name);
		long c = customer.getCustomerId();
		Status status;
		if(c == 0) {
			status = Status.BAD_REQUEST;	
		}
		else {
			status = Status.OK;
		}
		return Response.status(status).entity(customer).build();
	}


	
	
	//http://APP_URL/loja/api/rest/customer/create
	/* In body:
	 * {
		"name": "João da Silva Santos",
		"address": "Endereço de Teste",
		"telephone": "(11) 95432 - 3321",
		"email": "joao@uol.com.br",
		"password": "joao"		
	   }
	 */
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createCustomer(Customer customer) {
		long customerId = customerService.createCustomer(customer);
		Status status;
		if(customerId == 0) {
			status = Status.BAD_REQUEST;	
		}
		else {
			status = Status.OK;
		}		
		return Response.status(status).entity("{\"id\": " + customerId + "}").build();
	}
	
	
	//http://APP_URL/loja/api/rest/customer/update/1
	/* In body:
	 * {
		"name": "João da Silva Santos alerado",
		"address": "Endereço de Teste alterado",
		"telephone": "(11) 93321-5432",
		"email": "joao@bol.com.br",
		"password": "joao_da_Silva"				
	   }
	 */
	@PUT
	@Path("/update/{customerId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateCustomer(@PathParam("customerId") long customerId, Customer customer) {
		boolean updated = customerService.updateCustomer(customerId, customer);
		Status status;
		if(!updated) {
			status = Status.BAD_REQUEST;	
		}
		else {
			status = Status.OK;
		}		
		return Response.status(status).entity("{\"updated\": " +  updated + "}").build();
	}
	
	
	//http://APP_URL/loja/api/rest/customer/remove/3
	@DELETE
	@Path("/remove/{customerId}")
	public Response removeCustomer(@PathParam("customerId") long customerId) {
		boolean deleted = customerService.removeCustomer(customerId);
		Status status;
		if(!deleted) {
			status = Status.BAD_REQUEST;	
		}
		else {
			status = Status.OK;
		}		
		return Response.status(status).entity("{\"deleted\": " + deleted + "}").build();
	}
	
		
}
