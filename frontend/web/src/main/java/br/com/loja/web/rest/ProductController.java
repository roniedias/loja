package br.com.loja.web.rest;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.loja.backend.entity.Product;
import br.com.loja.backend.service.ProductService;


@Path("product")
@Produces(MediaType.APPLICATION_JSON)
public class ProductController {
	
	@EJB
	private ProductService productService;
	
	// http://APP_URL/loja/api/rest/product
	@GET
	@Path("")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response listProducts() throws Exception {
		return Response.status(Status.OK).entity(productService.listProducts()).build();
	}	
	
	// http://APP_URL/loja/api/rest/product/find/1
	@GET 
	@Path("/find/{productId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response findProductById(@PathParam("productId") int productId) throws Exception {
		Product product = productService.findProductById(productId);
		int p = product.getProductId();
		Status status;
		if(p == 0) {
			status = Status.BAD_REQUEST;	
		}
		else {
			status = Status.OK;
		}		
		return Response.status(status).entity(productService.findProductById(productId)).build();
	}

	
	
	//http://APP_URL/loja/api/rest/product/create
	/* In body:
	 * {
		"description": "Produto 1",
		"price": 60.00,
		"stock": 15
	   }
	 */
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createProduct(Product product) throws Exception {
		int productId = productService.createProduct(product);
		if(productId == 0) {
			return Response.status(Status.BAD_REQUEST).entity("{\"id\": " + productId + "}").build();
		}
		return Response.status(Status.OK).entity("{\"id\": " + productId + "}").build();
	}
	
	
	//http://APP_URL/loja/api/rest/product/update/1
	/* In body:
	 * {
		"description": "Produto 1 - Atualizado",
		"price": 65.00,
		"stock": 13
	   }
	 */
	@PUT
	@Path("/update/{productId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProduct(@PathParam("productId") int productId, Product product) throws Exception {
		boolean updated = productService.updateProduct(productId, product);
		if(!updated) {
			return Response.status(Status.BAD_REQUEST).entity("{\"update\": " +  updated + "}").build();
		}
		return Response.status(Status.OK).entity("{\"update\": " +  updated + "}").build();
	}
	
	
	//http://APP_URL/loja/api/rest/product/remove/6
	@DELETE
	@Path("/remove/{productId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeProduct(@PathParam("productId") int productId) throws Exception {
		boolean deleted = productService.removeProduct(productId);
		if(!deleted) {
			return Response.status(Status.BAD_REQUEST).entity("{\"delete\": " + deleted + "}").build();
		}
		return Response.status(Status.OK).entity("{\"delete\": " + deleted + "}").build();
	}
	
	
	
}

