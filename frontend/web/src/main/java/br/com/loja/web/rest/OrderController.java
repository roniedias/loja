package br.com.loja.web.rest;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.loja.backend.entity.Order;
import br.com.loja.backend.service.CustomerService;
import br.com.loja.backend.service.LoginService;
import br.com.loja.backend.service.OrderService;

@Path("order")
@Produces(MediaType.APPLICATION_JSON)
public class OrderController {
	
	@EJB
	OrderService orderService;
	
	@EJB
	CustomerService customerService;
	
	@EJB
	LoginService loginService;

	
	@Context
	HttpServletRequest request;
	
	@POST
	@Path("/create")
	public Response createOrder() {		
		Order order = orderService.createOrder(request);
		return Response.status(Status.OK).entity(order).build();
	}
	
	
	@GET
	@Path("/list_user_orders")
	public Response listOrdersByCustomerId() {
		long customerId = loginService.getLoggedUser(request);
		return Response.status(Status.OK).entity(orderService.listOrdersByCustomerId(customerId)).build();
	}

}
