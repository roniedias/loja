package br.com.loja.web.rest;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.loja.backend.entity.Customer;
import br.com.loja.backend.service.ChartService;
import br.com.loja.backend.service.CustomerService;
import br.com.loja.backend.service.LoginService;


@Path("login")
@Produces(MediaType.APPLICATION_JSON)
public class LoginController {
	
	@Context 
	HttpServletRequest request;
	
	@Context 
	HttpServletResponse response;
	
	@EJB
	LoginService loginService;
	
	@EJB
	CustomerService customerService;
	
	@EJB
	ChartService chartService;
	
	
	@POST
	@Path("")
	public Response login(@FormParam("user") String user, @FormParam("password") String password) throws ServletException, IOException {
			
		boolean logged = loginService.isValidLogin(user, password);
		
		Customer customer = customerService.findCustomerByEmail(user);
		
		if(logged) {
			request.getRequestDispatcher("/WEB-INF/protected-pages/order.html").forward(request, response);
			request.getSession(true).setAttribute("user_logged", customer.getCustomerId()); // In case to be necessary verify if user is logged or not. May be necessary to implement an interceptor (or something like) for this purpose
			return Response.status(Status.OK).entity("{\"logged\": " + logged + "}").build();
		}
		
		request.getRequestDispatcher("/WEB-INF/protected-pages/access-denied.html").forward(request, response);
		return Response.status(Status.FORBIDDEN).entity("{\"logged\": " + logged + "}").build();
	}	

	
	@POST
	@Path("invalidate")
	public Response logout() throws ServletException, IOException {
		chartService.removeChart(request);
		request.getSession(true).setAttribute("user_logged", null); // In case to be necessary verify if user is logged or not. May be necessary to implement an interceptor (or something like) for this purpose
		return Response.status(Status.OK).entity("{\"logged\": " + loginService.getLoggedUser(request) + "}").build();		
	}
	
	@GET
	@Path("logged")
	public Response getLoggedUser() {
		return Response.status(Status.OK).entity("{\"loggedUser\": " + loginService.getLoggedUser(request) + "}").build();
	}
	
	
		
}

