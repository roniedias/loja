$( document ).ready(function() {
	

	var chartProductsUrl = "http://52.67.83.75/loja/api/rest/chart";
	$.ajax({
		type: "GET",
		url: chartProductsUrl,
		contentType:"application/json",
		crossDomain: true,
		success: function(result){
			
			if(result.length == 0) {
				$('#id_create_order_button').hide();
			}
			
			var tr;
			for (var i = 0; i < result.length; i++) {	
				tr = $('<tr onClick="fillChartProducts(this)"/>');
				tr.append("<td>" + result[i].productId + "</td>");
				tr.append("<td>" + result[i].description + "</td>");
				tr.append("<td>" + result[i].price + "</td>");
				tr.append("<td>" + result[i].stock + "</td>");
				$('#tbody_chart_products').append(tr);
			}			
		 },
		 error:function(xhr,status,error){
		 	alert(xhr.responseText);
		 }
	});
	
	
	$('#id_btn_customer_order').click(function() {
		
		var userOrdersUrl = "http://52.67.83.75/loja/api/rest/order/list_user_orders";
		$.ajax({
			type: "GET",
			url: userOrdersUrl,
			contentType:"application/json",
			crossDomain: true,
			success: function(result){
				
				$("#tbody_customer_orders tr").remove();
				
				var tr;
				for (var i = 0; i < result.length; i++) {
					
					tr = $('<tr onClick="customerOrders(this)"/>');
					tr.append("<td>" + result[i].order.orderId + "</td>");
					
					var date = new Date(result[i].order.orderDate);
					tr.append("<td>" + formatDate(date) + "</td>");
					
					tr.append("<td>" + result[i].order.total + "</td>");
					$('#tbody_customer_orders').append(tr);
				}			
			 },
			 error:function(xhr,status,error){
				 bootbox.alert(xhr.responseText);
			 }
		});
			
	});
	
	
	
});




function formatDate(date) {
	
    var day = date.getDate();
    var month = date.getMonth() + 1; //January is 0!
    var year = date.getFullYear();
    
    if(day < 10){
        day = '0' + day;
    } 
    
    if(month < 10){
        month = '0' + month;
    }
    
    var formatted = day +'/' + month + '/' + year;
    return formatted;
}



function customerOrders(el) {
	
	var orderId;
	var orderDate;
	var total;
	
	var $el = $(el);
	$('td', $el).each(function(idx, obj) {
		if(idx == 0) {
			orderId = obj.innerHTML;
		}
		else if(idx == 1) {
			orderDate = obj.innerHTML;
		}
		else {
			total = obj.innerHTML;
		}
	});
}



function createOrder() {
	
	var createOrderUrl = "http://52.67.83.75/loja/api/rest/order/create";
	
		$.ajax({
			type: "POST",
			url: createOrderUrl,
			contentType:"application/json",
			crossDomain: true,
			success: function(result){
				bootbox.alert("Pedido realizado com sucesso!");
				$("#tbody_chart_products").remove();
			},
			error:function(xhr,status,error){
				alert(xhr.responseText);
			},
			dataType: 'json'
		});	
}


function logout() {
	
	var logoutUrl = "http://52.67.83.75/loja/api/rest/login/invalidate";
	
	$.ajax({
		type: "POST",
		url: logoutUrl,
		contentType:"application/json",
		crossDomain: true,
		success: function(result){
		},
		error:function(xhr,status,error){
			bootbox.alert(xhr.responseText);
		},
		dataType: 'json'
	});	
	
	window.location.replace("http://52.67.83.75/loja/")

}


