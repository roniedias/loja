$( document ).ready(function() {

	var url = "http://52.67.83.75/loja/api/rest/product";

	$.ajax({
		type: "GET",
		url: url,
		contentType:"application/json",
		crossDomain: true,
		success: function(result){
			var tr;
			for (var i = 0; i < result.length; i++) {	
				tr = $('<tr onClick="productSelected(this)"/>').addClass('product-tr-class');
				tr.append("<td>" + result[i].productId + "</td>");
				tr.append("<td>" + result[i].description + "</td>");
				tr.append("<td>" + result[i].price + "</td>");
				tr.append("<td>" + result[i].stock + "</td>");
				$('#tbody_products').append(tr);
			}			
		 },
		 error:function(xhr,status,error){
		 	alert(xhr.responseText);
		 }
	});
	
	refreshChartQuantity();
});


	
	function productSelected(el) {
		
		var productId;
		var description;
		var price;
		var stock;
		
		var $el = $(el);
		$('td', $el).each(function(idx, obj) {
			if(idx == 0) {
				productId = obj.innerHTML;
			}
			else if(idx == 1) {
				description = obj.innerHTML;
			}
			else if(idx == 2) {
				price = obj.innerHTML;
			}
			else {
				stock = obj.innerHTML;
			}
		});
		
		bootbox.confirm("Deseja adicionar este produto ao carrinho?", function(result) {
			if(result == true) {
				var chartAddProductUrl = "http://52.67.83.75/loja/api/rest/chart/add";
		  		var dta = {"productId": productId, "description": description, "price": price, "stock": stock };
		  		$.ajax({
		  			type: "POST",
		  			url: chartAddProductUrl,
		  			contentType:"application/json",
		  			crossDomain: true,
		  			success: function(result){
		  				bootbox.alert("Produto adicionado com sucesso ao carrinho!!!");
		  				increaseChartQuantity();
		  			},
		 			 error:function(xhr,status,error){
		 			 	console.log(xhr.responseText);
		        	},
		        	data: JSON.stringify(dta),
		        	dataType: 'json'
				});
			}
			
		}); 	
	}
	
	
	
	
	function increaseChartQuantity() {
		var quantity = parseInt($("#id_chart_quantity").html()) + 1;
		$("#id_chart_quantity").html(quantity);
	}
	
	
	function refreshChartQuantity() {
		
		var url = "http://52.67.83.75/loja/api/rest/chart/quantity";
		var res;

		$.ajax({
			type: "GET",
			url: url,
			contentType:"application/json",
			crossDomain: true,
			success: function(result){
				$("#id_chart_quantity").html(result.quantity);
			},
			 error:function(xhr,status,error){
			 	alert(xhr.responseText);
			 }
		});

		return res;	
	}

	
	
	function login() {
		
		var user = $("#id_login_user").val();
		var password = $("#id_login_password").val();
		
		var loginUrl = "http://52.67.83.75/loja/api/rest/login";
  		var data = {"user": user, "password": password};
  		$.ajax({
  			type: "POST",
  			url: loginUrl,
  			contentType:"application/json",
  			crossDomain: true,
  			success: function(result){

  			},
  			error:function(xhr,status,error) {
  				$(function () {
  					$('#id_alert_user_pass_wrong').fadeIn('slow', function () {
  						$(this).delay(1000).fadeOut('slow');
  					});
  				});
        	},
        	data: JSON.stringify(data),
        	dataType: 'json'
		});

	}
	
	
