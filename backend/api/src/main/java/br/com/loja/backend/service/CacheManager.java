package br.com.loja.backend.service;

public interface CacheManager {
	
	public final String TEN_MIN_CACHE = "genericCache";

	<T> T getValue(String cache, String key);
	<T> void setValue(String cache, String key, T obj);
	void clear(String cache, String key);
	void clear(String cache);
}