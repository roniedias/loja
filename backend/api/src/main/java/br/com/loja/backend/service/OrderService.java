package br.com.loja.backend.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.com.loja.backend.dto.OrderDTO;
import br.com.loja.backend.entity.Order;

public interface OrderService {
	Order createOrder(HttpServletRequest request);
	List<OrderDTO> listOrdersByCustomerId(long customerId);
}
