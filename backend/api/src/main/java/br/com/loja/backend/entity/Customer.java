package br.com.loja.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@org.hibernate.annotations.Table(appliesTo = "customer", indexes = { @Index(name = "CustomerNameI001", columnNames = { "name" }) })
@NamedQueries({
	@NamedQuery(name = Customer.QUERY_LIST_ALL_CUSTOMERS, query = "SELECT c FROM Customer c"),
	@NamedQuery(name = Customer.QUERY_LIST_CUSTOMERS_BY_EMAIL, query = "SELECT c FROM Customer c WHERE c.email = :email")
})
@Table(name = "customer")
public class Customer {
	
	public static final String QUERY_LIST_ALL_CUSTOMERS = "QUERY_LIST_ALL_CUSTOMERS";
	public static final String QUERY_LIST_CUSTOMERS_BY_EMAIL = "QUERY_LIST_CUSTOMERS_BY_EMAIL";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long customerId;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private String address;
	
	@Column(nullable = false)
	private String telephone;
	
	@Column(nullable = false)
	private String email;

	@Column(nullable = false)
	private String password;

	

	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}