package br.com.loja.backend.service;

import javax.servlet.http.HttpServletRequest;

public interface LoginService {
	boolean isValidLogin(String user, String password);
	long getLoggedUser(HttpServletRequest request);
	boolean logout(HttpServletRequest request);
}
