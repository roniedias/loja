package br.com.loja.backend.service;

import java.util.List;

import javax.ejb.Local;

import br.com.loja.backend.entity.Product;


@Local
public interface ProductService {
	List<Product> listProducts();
	int createProduct(Product Product);
	boolean updateProduct(int productId, Product product);
	boolean removeProduct(int productId);
	Product findProductById(int productId);
}
