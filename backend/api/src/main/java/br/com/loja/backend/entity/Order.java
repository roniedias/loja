package br.com.loja.backend.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@NamedQueries({
	@NamedQuery(name = Order.QUERY_LIST_ALL_ORDERS, query = "SELECT o FROM Order o"),
	@NamedQuery(name = Order.QUERY_LIST_BY_CUSTOMER_ID, query = "SELECT o FROM Order o WHERE o.customerId = :customerId")
})
@Table(name = "order_table")
public class Order {
	
	public static final String QUERY_LIST_ALL_ORDERS = "QUERY_LIST_ALL_ORDERS";
	public static final String QUERY_LIST_BY_CUSTOMER_ID = "QUERY_LIST_BY_CUSTOMER_ID"; 
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int orderId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date orderDate;
	
	
	@Column(nullable = false)
	private long customerId;
		
	@Column(nullable = false)
	private double total;
	

	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
	public Order(){}
	
	public Order(int orderId, Date orderDate, long customerId, double total) {
		super();
		this.orderId = orderId;
		this.orderDate = orderDate;
		this.customerId = customerId;
		this.total = total;
	}
	
	
	
	
	
	

}
