package br.com.loja.backend.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
	@NamedQuery(name = Product.QUERY_LIST_ALL_PRODUCTS, query = "SELECT p FROM Product p")
})
@Table(name = "product")
public class Product implements Serializable {

	private static final long serialVersionUID = -6331670685366641334L;

	public static final String QUERY_LIST_ALL_PRODUCTS = "QUERY_LIST_ALL_PRODUCTS";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int productId;
	
	@Column(nullable = false)
	private String description;
	
	@Column(nullable = false)
	private double price;
	
	@Column(nullable = false)
	private int stock;

	
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	
	public Product(){}
	
	public Product(int productId, String description, double price, int stock) {
		this.productId = productId;
		this.description = description;
		this.price = price;
		this.stock = stock;
	}
	
}