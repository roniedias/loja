package br.com.loja.backend.service;

import java.util.List;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;

import br.com.loja.backend.entity.Product;

@Local
public interface ChartService {
	List<Product> listChartProducts(HttpServletRequest request);
	List<Product> addProductToChart(Product product, HttpServletRequest request);
	boolean removeChart(HttpServletRequest request);
	List<Product> removeProductFromChart(int productId, HttpServletRequest request);
	int numberOfProducts(HttpServletRequest request);
}
