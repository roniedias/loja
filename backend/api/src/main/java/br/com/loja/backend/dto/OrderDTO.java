package br.com.loja.backend.dto;

import java.util.List;

import br.com.loja.backend.entity.Order;
import br.com.loja.backend.entity.Item;

public class OrderDTO {
	
	private Order order;
	private List<Item> items;
	
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
	
	public OrderDTO(){}
	
	public OrderDTO(Order order, List<Item> itms) {
		this.order = order;
		this.items = itms;
	}
	
	
}
