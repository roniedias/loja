package br.com.loja.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
	@NamedQuery(name = Item.QUERY_LIST_ALL_ITEMS, query = "SELECT i FROM Item i"),
	@NamedQuery(name = Item.QUERY_LIST_ITEMS_BY_ORDER_ID, query = "SELECT i FROM Item i WHERE i.orderId = :orderId")
})
@Table(name = "item")
public class Item {
	
	public static final String QUERY_LIST_ALL_ITEMS = "QUERY_LIST_ALL_ITEMS";
	public static final String QUERY_LIST_ITEMS_BY_ORDER_ID = "QUERY_LIST_ITEMS_BY_ORDER_ID";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int itemId;
	
	@Column(nullable = false)
	public int productId;

	@Column(nullable = false)
	private int quantity;
	
	@Column
	private double itemTotal;

	@Column
	private int orderId;

	
	
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public double getItemTotal() {
		return itemTotal;
	}
	public void setItemTotal(double itemTotal) {
		this.itemTotal = itemTotal;
	}
	
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	
	public Item(){}
		
	public Item(int productId, int quantity) {
		this.productId = productId;
		this.quantity = quantity;
	}

	
	
	
}
