package br.com.loja.backend.service;

import java.util.List;

import javax.ejb.Local;

import br.com.loja.backend.entity.Customer;

@Local
public interface CustomerService {
	List<Customer> listCustomers();
	long createCustomer(Customer customer);
	boolean updateCustomer(long customerId, Customer customer);
	boolean removeCustomer(long customerId);
	Customer findCustomerById(long customerId);
	Customer findCustomerByEmail(String email);
}
