package br.com.loja.backend.service;

import java.util.List;

import javax.ejb.Local;

import br.com.loja.backend.entity.Item;

@Local
public interface ItemService {
	List<Item> listItems();
	Item findItemById(int itemId);
	int createItem(Item item);
	boolean updateItem(int itemId, Item item);
	boolean removeItem(int itemId);

}
