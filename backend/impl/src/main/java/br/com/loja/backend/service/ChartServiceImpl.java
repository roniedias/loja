package br.com.loja.backend.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.com.loja.backend.entity.Product;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ChartServiceImpl implements ChartService {

	
	//private static final Logger log = LoggerFactory.getLogger(ChartServiceImpl.class);
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> listChartProducts(HttpServletRequest request) {
		
		List<Product> chartProductsList = new ArrayList<Product>();
		
		HttpSession session = request.getSession(true);
		Object chartProducts = session.getAttribute("chart_products");
		
		if(chartProducts != null) {
			chartProductsList = (List<Product>) chartProducts;
		}		
		return chartProductsList;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> addProductToChart(Product product, HttpServletRequest request) {
		
		List<Product> chartProductsList = new ArrayList<Product>();
		
		HttpSession session = request.getSession(true);
		Object chartProducts = session.getAttribute("chart_products");
				
		if(chartProducts != null) {
			chartProductsList = (List<Product>) chartProducts;
		}
		
		if(product != null) {
			chartProductsList.add(product);
			session.setAttribute("chart_products", chartProductsList);
		}		
		
		return chartProductsList;
	}
	
	
	@Override
	public boolean removeChart(HttpServletRequest request) {
		request.getSession(true).setAttribute("chart_products", null);
		return true;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> removeProductFromChart(int productId, HttpServletRequest request) {
		
		List<Product> chartProductsList = new ArrayList<Product>();
		
		HttpSession session = request.getSession(true);
		Object chartProducts = session.getAttribute("chart_products");
				
		if(chartProducts != null) {
			chartProductsList = (List<Product>) chartProducts;
		}
		
		int initialSize = chartProductsList.size();
		
		for(Product p : chartProductsList) {
			if(p.getProductId() == productId) {
				chartProductsList.remove(p);
				break;
			}
		}
		
		if(chartProductsList.size() > 0) {
			session.setAttribute("chart_products", chartProductsList);
		}
		
		if(chartProductsList.size() == initialSize) {
			Product product = new Product(0, "No product removed from chart!", 0, 0);
			List<Product> products = new ArrayList<Product>();
			products.add(product);
			return products;
		}
				
		return chartProductsList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public int numberOfProducts(HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		Object chartProducts = session.getAttribute("chart_products");
		List<Product> chartProductsList = new ArrayList<Product>();
		if(chartProducts != null) {
			chartProductsList = (List<Product>) chartProducts;
			return chartProductsList.size();
		}
		return 0;
	}


	
}
