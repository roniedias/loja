package br.com.loja.backend.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.loja.backend.entity.Customer;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CustomerServiceImpl implements CustomerService {

	@PersistenceContext
	private EntityManager em;

	private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Override
	public List<Customer> listCustomers() {
		List<Customer> customers = new ArrayList<Customer>();
		customers = em.createNamedQuery(Customer.QUERY_LIST_ALL_CUSTOMERS, Customer.class).getResultList();
		return customers;
	}

	@Override
	public Customer findCustomerById(long customerId) {
		Customer customer = em.find(Customer.class, customerId);
		if (customer == null) {
			customer = new Customer();
			customer.setCustomerId(0);
			customer.setName("Invalid id provided for customer!");
		}
		return customer;
	}
	
	@Override
	public Customer findCustomerByEmail(String email) {
		List<Customer> customers = em.createNamedQuery(Customer.QUERY_LIST_CUSTOMERS_BY_EMAIL, Customer.class).setParameter("email", email).getResultList();
		Customer customer = new Customer();
		if(customers.size() == 0) {
			customer.setCustomerId(0);
			customer.setName("Invalid name provided for customer!");
		}
		else {
			customer = customers.iterator().next();
		}
		return customer;
	}
	

	@Override
	public long createCustomer(Customer customer) {
		long customerId = 0;
		if (customer != null) {
			em.persist(customer);
			customerId = customer.getCustomerId();
		}
		return customerId;
	}

	@Override
	public boolean updateCustomer(long customerId, Customer customer) {

		boolean updated = false;

		Customer c = em.find(Customer.class, customerId);
		
		if (c == null) {
			return false; // Invalid data provided in request body
		}

		try {
			c.setAddress(customer.getAddress());
			c.setName(customer.getName());
			c.setTelephone(customer.getTelephone());
			em.persist(c);
			updated = true;
		} catch (NullPointerException e) {
			log.info(e.getMessage());
			return false;
		}
		return updated;
	}

	@Override
	public boolean removeCustomer(long customerId) {

		boolean removed = false;

		Customer c = em.find(Customer.class, customerId);

		if (c == null) { // Invalid id provided for customer
			return false;
		} else {
			em.remove(c);
			removed = true;
		}
		return removed;
	}
	

}
