package br.com.loja.backend.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.loja.backend.entity.Item;
import br.com.loja.backend.entity.Product;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ItemServiceImpl implements ItemService {

	@PersistenceContext
	private EntityManager em;
	
	private static final Logger log = LoggerFactory.getLogger(ItemServiceImpl.class);

	@Override
	public List<Item> listItems() {
		List<Item> items = new ArrayList<Item>();
		items = em.createNamedQuery(Item.QUERY_LIST_ALL_ITEMS, Item.class).getResultList();
		return items;
	}

	@Override
	public Item findItemById(int itemId) {
		Item item = em.find(Item.class, itemId);
		if (item == null) {
			item = new Item();
		}
		return item;
	}

	@Override
	public int createItem(Item item) {
		int itemId = 0;
		if (item != null) {
			int productId = item.getProductId();
			Product p = em.find(Product.class, productId);
			if(p != null) {
				em.persist(item);
				itemId = item.getItemId();			
			}
		}
		return itemId;
	}

	@Override
	public boolean updateItem(int itemId, Item item) {

		boolean updated = false;

		Item i = em.find(Item.class, itemId);
		
		if (i == null) {
			return false; // Invalid data provided in request body
		}

		try {
			i.setProductId(item.getProductId());
			i.setQuantity(item.getQuantity());
			em.persist(i);
			updated = true;
		} catch (NullPointerException e) {
			log.info(e.getMessage());
			return false;
		}
		return updated;
	}

	@Override
	public boolean removeItem(int itemId) {

		boolean removed = false;

		Item i = em.find(Item.class, itemId);
		
		if (i == null) { // Invalid id provided for product
			return false;
		} else {
			em.remove(i);
			removed = true;
		}
		return removed;
	}
	
	
}

