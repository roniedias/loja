package br.com.loja.backend.service;


import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.infinispan.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
@Named
public class CacheManagerImpl implements CacheManager {

	private static final Logger log = LoggerFactory.getLogger(CacheManagerImpl.class);

	@Resource(lookup = "java:jboss/infinispan/cache/genericCache")
	private Cache<String, Object> genericCache;

	private Map<String, Cache<String, Object>> caches;

	@PostConstruct
	public void init() {
		caches = new HashMap<String, Cache<String, Object>>();
	}

	@SuppressWarnings("unchecked")
	private Cache<String, Object> getCache(String cache) {
		Cache<String, Object> c = caches.get(cache);
		if (c == null) {
			synchronized (this) {
				c = caches.get(cache);
				if (c == null) {
					try {
						c = (Cache<String, Object>) (new InitialContext().lookup("java:jboss/infinispan/cache/" + cache));
					} catch (NamingException e) {
						log.error("Cache " + cache + " not found in JBoss, using generic", e);
						c = genericCache;
					}
					c.clear();
					caches.put(cache, c);
				}
			}
		}
		return c;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getValue(String cache, String key) {
		return (T) getCache(cache).get(key);
	}

	@Override
	public <T> void setValue(String cache, String key, T obj) {
		getCache(cache).put(key, obj);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void clear(String cache, String key) {
		getCache(cache).removeAsync(key);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void clear(String cache) {
		getCache(cache).clearAsync();
	}
}

