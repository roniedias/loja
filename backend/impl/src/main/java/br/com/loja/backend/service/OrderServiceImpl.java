package br.com.loja.backend.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import br.com.loja.backend.dto.OrderDTO;
import br.com.loja.backend.entity.Item;
import br.com.loja.backend.entity.Order;
import br.com.loja.backend.entity.Product;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class OrderServiceImpl implements OrderService {
	
	@EJB
	ChartService chartService;
	
	@EJB
	ProductService productService;
	
	@EJB
	ItemService itemService;
	
	@PersistenceContext
	EntityManager em;
	
	@EJB
	CustomerService customerService;
	
	@EJB
	LoginService loginService;
	
	//private static final Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);
	
	@Override
	public Order createOrder(HttpServletRequest request) {
		
		long customerId = loginService.getLoggedUser(request);
		
		List<Item> items = convertChartProductsToItems(request);
		
		if(items.size() == 0) {
			return null;
		}
		
		Order order = new Order();
		order.setCustomerId(customerId);
		order.setOrderDate(new Date());
		
		double total = 0;
		
		for(Item i : items) {
			Product p = productService.findProductById(i.getProductId());
			
			if(i.getQuantity() > p.getStock()) {
				return null; // Stock not enough form items
			}
			else {
				// Updating product stock
				p.setStock(p.getStock() - i.getQuantity());
				productService.updateProduct(p.getProductId(), p);
			}
			
			double itemTotal = p.getPrice() * i.getQuantity();
			i.setItemTotal(itemTotal);
			total += itemTotal;
		}
		
		order.setTotal(total);
		
		em.persist(order);
		
		for(Item i : items) {
			i.setOrderId(order.getOrderId());
			itemService.createItem(i);
		}
		
		//chartService.removeChart(request);
		
		return order;
	}
	
	
	private List<Item> convertChartProductsToItems(HttpServletRequest request) {
		List<Product> chartProducts = chartService.listChartProducts(request);
		List<Item> items = new ArrayList<Item>();
		for(Product p : chartProducts) {
			if(items.size() == 0) {
				items.add(new Item(p.getProductId(), 1));
			}
			else {
				if(isProductInList(p.getProductId(), items)) {
					for(Item i : items) {
						if(i.getProductId() == p.getProductId()) {
							i.setQuantity(i.getQuantity() + 1);
						}
					}
				}
				else {
					items.add(new Item(p.getProductId(), 1));
				}
			}
		}	
		return items;
	}
		
		
	private boolean isProductInList(int productId, List<Item> items) {
		boolean exists = false;
		for(Item i : items) {
			if(i.productId == productId) {
				exists = true;
			}
		}
		return exists;
	}
	
	
	@Override
	public List<OrderDTO>listOrdersByCustomerId(long customerId) {

		if(customerId == 0) {
			return null;
		}
		List<Order> orders = em.createNamedQuery(Order.QUERY_LIST_BY_CUSTOMER_ID, Order.class).setParameter("customerId", customerId).getResultList();
		List<OrderDTO> oDto = new ArrayList<OrderDTO>();
		
		List<Item> items;
		
		if(orders.size() > 0) {
			for(Order o : orders) {
				items = em.createNamedQuery(Item.QUERY_LIST_ITEMS_BY_ORDER_ID, Item.class).setParameter("orderId", o.getOrderId()).getResultList();
				oDto.add(new OrderDTO(o, items));
			}	
		}
				
		return oDto;
	}
	
		

}
