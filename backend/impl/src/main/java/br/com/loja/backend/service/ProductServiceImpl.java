package br.com.loja.backend.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.loja.backend.entity.Product;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ProductServiceImpl implements ProductService {

	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private CacheManager cache;
	
	private static final String PRODUCTS_KEY = "PRODUCTS_KEY";


	private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

	@Override
	public List<Product> listProducts() {
		
		List<Product> products = cache.getValue(CacheManager.TEN_MIN_CACHE, PRODUCTS_KEY);
		
		if (products == null || products.size() == 0) {
			products = em.createNamedQuery(Product.QUERY_LIST_ALL_PRODUCTS, Product.class).getResultList();
			cache.setValue(CacheManager.TEN_MIN_CACHE, PRODUCTS_KEY, products);
		}
		return products;
	}

	@Override
	public Product findProductById(int productId) {
		List<Product> products = cache.getValue(CacheManager.TEN_MIN_CACHE, PRODUCTS_KEY);
		Product product = null;
		
		if (products == null || products.size() == 0) {
			product = em.find(Product.class, productId, LockModeType.PESSIMISTIC_READ);
		}
		else {
			for(Product p : products) {
				if(p.getProductId() == productId) {
					product = p;
				}
			}
		}

		if (product == null) {
			product = new Product();
			product.setProductId(0);
			product.setDescription("Invalid id provided for product!");
		}			
		
		return product;
	}

	@Override
	public int createProduct(Product product) {
		int productId = 0;
		if (product != null) {
			em.persist(product);
			productId = product.getProductId();
		}
		cache.clear(CacheManager.TEN_MIN_CACHE, PRODUCTS_KEY);
		return productId;
	}

	@Override
	public boolean updateProduct(int productId, Product product) {

		boolean updated = false;

		Product p = em.find(Product.class, productId);
		
		if (p == null) {
			return false; // Invalid data provided in request body
		}

		try {
			p.setDescription(product.getDescription());
			p.setPrice(product.getPrice());
			p.setStock(product.getStock());
			em.merge(p);
			em.flush();
			updated = true;
			cache.clear(CacheManager.TEN_MIN_CACHE, PRODUCTS_KEY);
		} catch (NullPointerException e) {
			log.info(e.getMessage());
			return false;
		}
		return updated;
	}

	@Override
	public boolean removeProduct(int productId) {

		boolean removed = false;

		Product p = em.find(Product.class, productId);

		if (p == null) { // Invalid id provided for product
			return false;
		} else {
			em.remove(p);
			removed = true;
			cache.clear(CacheManager.TEN_MIN_CACHE, PRODUCTS_KEY);
		}

		return removed;
	}
	
	
}
