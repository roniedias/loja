package br.com.loja.backend.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.com.loja.backend.entity.Customer;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class LoginServiceImpl implements LoginService {
	
	@PersistenceContext
	private EntityManager em;
	
	@EJB 
	CustomerService customerService;
	
	//private static final Logger log = LoggerFactory.getLogger(LoginServiceImpl.class);
	
	@Override
	public boolean isValidLogin(String user, String password) {
		
		List<Customer> customers = em.createNamedQuery(Customer.QUERY_LIST_ALL_CUSTOMERS, Customer.class).getResultList();
		
		if(customers.size() == 0) {
			return false;
		}
		
		for(Customer c : customers) {
			if(c.getEmail().trim().toLowerCase().equals(user.trim().toLowerCase()) && c.getPassword().trim().toLowerCase().equals(password.trim().toLowerCase())) {
				return true;
			}
 		}
		
		return false;
	}
	
	
	@Override
	public long getLoggedUser(HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		if(session.getAttribute("user_logged") == null) {
			return 0;
		}
		return (long) session.getAttribute("user_logged");
	}
	
	
	@Override
	public boolean logout(HttpServletRequest request) {
		request.getSession(true).setAttribute("user_logged", null);
		return true;
	}

	



}
